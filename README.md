# selinux-policy-targeted-collectd-hw-plugins

SElinux addons for collectd hardware plugins at CERN

## Upgrading
In case of upgrade of this package, don't forget to bump up the `.te` module file version to avoid SELinux complaining.
## Docs
[Fedora SELinux packaging docs](https://fedoraproject.org/wiki/SELinux_Policy_Modules_Packaging_Draft?rd=PackagingDrafts/SELinux/PolicyModules)
