%global modulename collectd_hw_plugins

%if 0%{rhel} < 7
%define restorecon /sbin/restorecon
%else
%define restorecon /usr/sbin/restorecon
%endif

Name:    selinux-policy-targeted-collectd-hw-plugins
Version: 22.2.3
Release: 1%{?dist}
Summary: Selinux policy addons for collectd CERN HW plugins
Group:   System Environment/Base
Vendor:  CERN
License: MIT
URL:     https://gitlab.cern.ch/hw/selinux-policy-targeted-collectd-hw-plugins
Source0: %{name}-%{version}.tgz

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildRequires: checkpolicy
BuildRequires: selinux-policy
BuildRequires: bzip2
BuildRequires: selinux-policy-devel
Requires: selinux-policy-targeted
Requires: collectd
Requires: cerncollectd-selinux
Requires: smart-directives-handler
Requires: collectd-smart-tests
Requires: collectd-megaraidsas
Requires(post): /usr/sbin/semodule
Requires(post): %{restorecon}
Requires(postun): /usr/sbin/semodule
Requires(postun): %{restorecon}


%description
SELinux policy addon for collectd CERN HW plugins

%global debug_package %{nil}

%prep
%setup -q

%build
(cd src/%{rhel}/
 make -f %{_datadir}/selinux/devel/Makefile
 bzip2 *.pp
)


%install
%{__rm} -rf %{buildroot}
%{__mkdir} -p %{buildroot}%{_datadir}/selinux/targeted
install -m 0644 src/%{rhel}/%{modulename}.pp.bz2 %{buildroot}%{_datadir}/selinux/targeted/%{modulename}.pp.bz2


%clean
%{__rm} -rf %{buildroot}


%post
/usr/sbin/semodule -s targeted -i %{_datadir}/selinux/targeted/%{modulename}.pp.bz2 &> /dev/null || :
%{restorecon} -RF %{_var}/cache/collectd-megaraidsas &> /dev/null || :
%{restorecon} -RF %{_var}/cache/smart-directives-handler &> /dev/null || :
%{restorecon} -RF %{_var}/cache/common-hardwaretools-libs &> /dev/null || :
%{restorecon} -RF %{_var}/log/blockdevice_drivers_errors\.log &> /dev/null || :
%{restorecon} -RF %{_var}/log/smart-directives-handler\.log &> /dev/null || :
%{restorecon} -RF %{_var}/log/s2cli\.log &> /dev/null || :

/sbin/service collectd condrestart >/dev/null 2>&1 || :


%postun
if [ $1 -eq 0 ] ; then
    /usr/sbin/semodule -s targeted -r %{modulename} &> /dev/null || :
    %{restorecon} -RF %{_var}/cache/collectd-megaraidsas &> /dev/null || :
    %{restorecon} -RF %{_var}/cache/smart-directives-handler &> /dev/null || :
    %{restorecon} -RF %{_var}/cache/common-hardwaretools-libs &> /dev/null || :
    %{restorecon} -RF %{_var}/log/blockdevice_drivers_errors\.log &> /dev/null || :
    %{restorecon} -RF %{_var}/log/smart-directives-handler\.log &> /dev/null || :
    %{restorecon} -RF %{_var}/log/s2cli\.log &> /dev/null || :

fi


%triggerpostun -- selinux-policy-targeted-collectd-hw-plugins < 19.1.2
/usr/sbin/semodule -s targeted -i %{_datadir}/selinux/targeted/%{modulename}.pp.bz2 &> /dev/null || :
%{restorecon} -RF %{_var}/cache/collectd-megaraidsas &> /dev/null || :
%{restorecon} -RF %{_var}/cache/smart-directives-handler &> /dev/null || :
%{restorecon} -RF %{_var}/cache/common-hardwaretools-libs &> /dev/null || :
%{restorecon} -RF %{_var}/log/blockdevice_drivers_errors\.log &> /dev/null || :
%{restorecon} -RF %{_var}/log/smart-directives-handler\.log &> /dev/null || :
%{restorecon} -RF %{_var}/log/s2cli\.log &> /dev/null || :



%files
%defattr(-,root,root,-)
%{_datadir}/selinux/targeted/%{modulename}.pp.bz2
%doc AUTHORS COPYING


%changelog
* Mon Feb 14 2022 - Luca Gardi <luca.gardi@cern.ch> - 22.2.3-1
- add kmod_exec_t permissions needed for lsmod execution
- add var_log_t needed to read /var/log/messages

* Thu Jan 13 2022 - Luca Gardi <luca.gardi@cern.ch> - 22.1.3-1
- deploy on CentOS 9 Stream

* Wed Apr 14 2021 - Luca Gardi <luca.gardi@cern.ch> - 21.4.3-1
- deploy on c8s
- drop slc6 support

* Tue Oct 27 2020 - Luca Gardi <luca.gardi@cern.ch> - 20.10.5-1
- deploy using rpmci
- compatibility with Centos8

* Thu Jan 16 2020 - Luca Gardi <luca.gardi@cern.ch> - 20.1.3-1
- spec: add restorecon on chlib cache folder

* Tue Oct 15 2019 - Luca Gardi <luca.gardi@cern.ch> - 19.10.3-1
- allow access to common-hardwaretools-libs cache

* Tue Apr 02 2019 - Luca Gardi <luca.gardi@cern.ch> - 19.4.1-2
- collectd_t: allow puppet_etc_t:file (getattr read open), process (setpgid)

* Fri Mar 29 2019 - Luca Gardi <luca.gardi@cern.ch> - 19.3.5-1
- allow scsi_generic_device_t read open ioctl write 
- allow ipmi_device_t read write open ioctl allow var_lock_t read for facter
- add s2cli.log to collectd_var_log_t
- add s2cli.log to collectd_var_log_t

* Wed Mar 06 2019 - Luca Gardi <luca.gardi@cern.ch> - 19.3.1-3
- remove Require(triggerpostun)
- add dependecy on cerncollectd-selinux
- enhance makefile
- add smart-directives handler to collectd_var_log_t label
- pipe restorecon errors to /dev/null 
- add smart-directives-handler log file label

* Tue Feb 12 2019 - Luca Gardi <luca.gardi@cern.ch> - 19.2.3-1
- separate rules for SLC6
- reload collectd on update
- rules for var_log_t context for blockdevice-drivers-errors
- rules for device_t chr_file for megaraidsas

* Tue Jan 15 2019 - Luca Gardi <luca.gardi@cern.ch> - 19.1.3-1
- add specfile triggerpostun section to avoid unwanted SE module unload 
  from old broken preun section
- add proper update check on specfile postun section
- new collectd_cache_t context for smart_tests and collectd-megaraidsas
- file transition rule for caching folders
- domain transition rules for smartctl and lsmod use
- dontaudit rules for lsscsi chr_file getattrs
- add dependency on smart-directives-handler and collectd-megaraidsas

* Mon Jan 07 2019 - Luca Gardi <luca.gardi@cern.ch> - 19.1.2-1
- introduce permissions needed by collectd-megaraidsas
- allow read/writes on var_t context for caching

* Wed Nov 21 2018 - Luca Gardi <luca.gardi@cern.ch> - 18.11.4-1
- Initial release
